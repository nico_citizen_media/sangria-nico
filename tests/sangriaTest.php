<?php 

require __DIR__ . '/../Sangria.php';

class SangriaTest extends PHPUnit_Framework_TestCase
{
    protected $sangria;

    public function setUp()
    {
        $this->sangria = new Sangria;
    }


    public function testFindGroup()
    {
        $this->assertEquals($this->sangria->findGroup("hablar"), 1);
        $this->assertEquals($this->sangria->findGroup("hacer"), 2);
        $this->assertEquals($this->sangria->findGroup("venir"), 3);
    }


    public function testConjugate()
    {
        $this->assertEquals($this->sangria->conjugate("hablar", "yo"), "hablo");
        $this->assertEquals($this->sangria->conjugate("hacer", "él"), "hace");
        $this->assertEquals($this->sangria->conjugate("venir", "vosotros"), "venís");
    }
}
