<?php 

class Sangria
{

    public function findGroup($v)
    {
        $term = substr($v, -2);

        if ($term == "ar") {
            return 1;
        } else if ($term == "er") {
            return 2;
        } else {
            return 3;
        }
    }

    public function findRoot($v)
    {
        return substr($v, 0, -2);
    }

    public function conjugate($v, $p) {

        $group = $this->findGroup($v);

        $termsByGroup[1] = array("yo"=>"o","tú"=>"as","él"=>"a","nosotros"=>"amos","vosotros"=>"áis","ellos"=>"an");
        $termsByGroup[] = array("yo"=>"o","tú"=>"es","él"=>"e","nosotros"=>"emos","vosotros"=>"éis","ellos"=>"en");
        $termsByGroup[] = array("yo"=>"o","tú"=>"es","él"=>"e","nosotros"=>"imos","vosotros"=>"ís","ellos"=>"en");

        return sprintf("%s%s", $this->findRoot($v), $termsByGroup[$group][$p]);
    }
}
